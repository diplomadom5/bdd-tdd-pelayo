import re

def validar_correo(correo):
    # Expresión regular para validar el formato de correo electronico
    patron = r'^[\w\.-]+@[\w\.-]+\.\w+$'

    return re.match(patron, correo) is not None
