import re

def validar_correo(correo):
    # Expresión regular para validar el formato de correo electrónico
    patron_correo = r'^[\w\.-]+@[\w\.-]+\.\w+$'
    
    # Verificar si el correo coincide con el patrón
    return bool(re.match(patron_correo, correo))

# Ejemplo de uso
correo_valido = "usuario@example.com"
correo_invalido = "correo_invalido"

if validar_correo(correo_valido):
    print(f"'{correo_valido}' es un correo electrónico válido.")
else:
    print(f"'{correo_valido}' no es un correo electrónico válido.")

if validar_correo(correo_invalido):
    print(f"'{correo_invalido}' es un correo electrónico válido.")
else:
    print(f"'{correo_invalido}' no es un correo electrónico válido.")
