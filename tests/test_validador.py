import unittest
from core.validador import validar_correo

class TestValidadorCorreo(unittest.TestCase):

    def test_validar_correo_valido(self):
        correo_valido = "usuario@example.com"
        resultado = validar_correo(correo_valido)
        self.assertTrue(resultado)

    def test_validar_correo_invalido(self):
        correo_invalido = "correo_invalido"
        resultado = validar_correo(correo_invalido)
        self.assertFalse(resultado)

if __name__ == '__main__':
    unittest.main()
