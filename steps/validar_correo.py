from behave import given, when, then
from validador import validar_correo

@given('que tengo un correo electronico "{correo}"')
def step_given_correo(context, correo):
    context.correo = correo

@when('lo valido')
def step_when_valido(context):
    context.es_valido = validar_correo(context.correo)

@then('debe ser valido')
def step_then_debe_ser_valido(context):
    assert context.es_valido, f"'{context.correo}' no es un correo electronico valido"

@then('debe ser invalido')
def step_then_debe_ser_invalido(context):
    assert not context.es_valido, f"'{context.correo}' es un correo electronico valido, pero se esperaba que fuera invalido"
