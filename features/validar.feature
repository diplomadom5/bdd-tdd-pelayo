Feature: Validacion de correo electronico
  Como un usuario del sistema
  Quiero validar correos electronicos
  Para asegurarme de que son volidos
  Scenario Outline: Validar correos electronicos validos
    Given que tengo un correo electronico "<correo>"
    When lo valido
    Then debe ser valido

  Scenario Outline: Validar correos electronicos invalidos
    Given que tengo un correo electronico "<correo>"
    When lo valido
    Then debe ser invalido

  Examples:
    | correo                             |
    | usuario@.com                       |
    | correo_invalido                    |
    | @dominio.com                       |
    | usaurio@usuario@example.com        |
    | usuario.com.bo                     |
